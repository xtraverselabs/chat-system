package com.jetbrains;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;
import static java.lang.System.*;

/**
 *
 * @author Tshiamo Komane BE2015-1285
 */
public class Server {

    private int port;
    private ServerSocket sSocket;
    private Socket cSocket;
    private DataInputStream instream;
    private DataOutputStream outstream;

    //get port from user
    private void setPort(){
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the port number you would like to use: ");
        this.port = in.nextInt();

    }

    private void createServerSocket() throws IOException {
        out.println("SYSTEM: creating server socket - you may establish a connection on the client side");
        sSocket = new ServerSocket(Server.this.port);
    }

    private void listen() throws IOException {
        cSocket = sSocket.accept();
    }

    private void establishStreams() throws IOException {
        instream = new DataInputStream(cSocket.getInputStream());
        outstream = new DataOutputStream(cSocket.getOutputStream());
    }

    private void writeMsg(String msg) throws IOException {
        DataOutputStream outstream;
        outstream = new DataOutputStream(cSocket.getOutputStream());
        outstream.writeUTF(msg);
    }

    private String getMsg() throws IOException {
        DataInputStream instream;
        instream = new DataInputStream(cSocket.getInputStream());
        return instream.readUTF();
    }



    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(in);
        String breakMsg = "quit";
        Server server1 = new Server();
        server1.setPort();
        server1.createServerSocket();


        out.println("SYSTEM: connecting to client");
        out.println("SYSTEM: type 'quit' when you are ready to end the chat");
        while (true) {
            server1.listen();
            server1.establishStreams();

            //receiving
            out.println("Client: ");
            String clientMsg = server1.getMsg();
            out.println(clientMsg);

            //sending
            out.println("Server: ");
            String msg = scan.nextLine();
            if (Objects.equals(msg, breakMsg)) {
                System.out.println("SYSTEM: GOOD BYE");
                break;
            }
            else server1.writeMsg(msg);

        }

    }

}
