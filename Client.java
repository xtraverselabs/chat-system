
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Braulio Bernardo
 */
public class Client {
    
    private Socket socket;
    private boolean isRunning = true;
    private String serverInfo;
    private final String ip = "127.0.0.7", exitCommand = "exit";
    private final int port = 1342;
    
    public void setServerInfo() {
        serverInfo = "------------------------------\n";
        serverInfo += "CLIENT MACHINE \n";
        serverInfo += "SERVER LOCATION: " + ip + "\n";
        serverInfo += "PORT NUMBER: " + port + "\n";
        serverInfo += "------------------------------\n";
    }
    
    public String getServerInfo() {
        return serverInfo;
    }
    
    public void establishConnection() throws IOException {        
        socket = new Socket(ip, port);
    }
    
    public void writeToServer(String message) throws IOException {
        DataOutputStream output; 
        output = new DataOutputStream(socket.getOutputStream());
        output.writeUTF(message);
    }
    
    public String getServerMessage() throws IOException {
        DataInputStream input; 
        input = new DataInputStream(socket.getInputStream());
        return input.readUTF();
    }
    
    public void abortIfServerSendsExitCommand(String msg) {
        if (exitCommand.equals(msg)) {
            isRunning = false;
        }
    }
    
    public static void print(String message) {
        System.out.print(message);
    }
    
    public static void println(String message) {
        System.out.println(message);
    }    
    
    public static void main(String[] args) throws IOException {
        
        Scanner scanner = new Scanner(System.in);
        Client client = new Client();
        String clientMsg, serverMsg, exceptionMsg;
        
        client.setServerInfo();
        println(client.getServerInfo());
        
        while(client.isRunning) {                                    
            try {                
                // input
                client.establishConnection();
                print("Me: ");
                clientMsg = scanner.nextLine();
                client.writeToServer(clientMsg);
                
                // output
                print("Server: ");                
                serverMsg = client.getServerMessage();                
                print(serverMsg);
                
                client.abortIfServerSendsExitCommand(serverMsg);
                println("");                
            } catch (IOException e) {                
                exceptionMsg = "ERROR: Make sure the server is running first\n";
                exceptionMsg += "Also check that the socket host/port is valid";
                println(exceptionMsg);
                break;
            }
        }
    }    
}
